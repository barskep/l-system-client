﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Object = System.Object;
using Task = Domain.Task;

namespace Server
{
    internal enum Method
    {
        Get,
        Post,
        Patch,
        Delete,
    }

    public class ServerApi : LSystemApi
    {
        private const string BaseUrl = "http://25.84.141.212:8880";

        private string _username;
        private string _password;

        public void SignIn(string login, string password, Action<Response<User>> callback)
        {
            var base64Credentials = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{login}:{password}"));
            var headers = new Dictionary<string, object>()
            {
                {"Authorization", "Basic " + base64Credentials},
            };

            Request<User>(Method.Get, Endpoint.SIGN_IN, headers, null, (userResponse) =>
            {
                if (!userResponse.IsError)
                {
                    _username = userResponse.Data.username;
                    _password = password;
                }

                callback(userResponse);
            });
        }

        public void SignUp(string login, string name, string password, string role, Action<Response<User>> callback)
        {
            var body = new Dictionary<string, string>
            {
                {"username", login}, {"name", name}, {"password", password}, {"role", role},
            };

            var json = DictionaryToJson(body);

            Request<User>(Method.Post, Endpoint.SIGN_UP, null, json, (userResponse) =>
            {
                if (!userResponse.IsError)
                {
                    _username = userResponse.Data.username;
                    _password = password;
                }

                callback(userResponse);
            });
        }

        public void SignUpStudent(string login, string name, string password, string teacherId, string info,
            Action<Response<User>> callback)
        {
            var body = new Dictionary<string, string>
            {
                {"username", login},
                {"name", name},
                {"password", password},
                {"role", "student"},
                {"teacherId", teacherId},
                {"info", info}
            };

            var json = DictionaryToJson(body);

            Request<User>(Method.Post, Endpoint.SIGN_UP, null, json, callback);
        }

        public void EditStudent(string id, string login, string name, string password, string info,
            Action<Response<User>> callback)
        {
            var body = new Dictionary<string, string>
            {
                {"username", login},
                {"name", name},
                {"password", password},
                {"info", info}
            };

            var json = DictionaryToJson(body);

            Request<User>(Method.Patch, $"{Endpoint.STUDENTS}?id={id}", null, json, callback);
        }

        public void GetStudents(string teacherId, Action<Response<Wrapper<User>>> callback)
        {
            Request<Wrapper<User>>(Method.Get, $"{Endpoint.STUDENTS}?teacherId={teacherId}", null, null, callback);
        }

        public void GetTasks(string teacherId, Action<Response<Wrapper<Task>>> callback)
        {
            var body = new Dictionary<string, string>()
            {
                {"teacherId", teacherId},
            };
            var jsonBody = DictionaryToJson(body);
            Request<Wrapper<TaskWrapper>>(Method.Get, $"{Endpoint.TASKS}?teacherId={teacherId}", null, jsonBody,
                resp =>
                {
                    Response<Wrapper<Task>> response;
                    if (!resp.IsError)
                    {
                        var taskList = resp.Data.items.ToList().ConvertAll((taskWrapper) => new Task()
                        {
                            text = taskWrapper.text,
                            taskId = taskWrapper.taskId,
                            gameField = taskWrapper.gameField.Split('&'),
                        });
                        response = new Response<Wrapper<Task>>(new Wrapper<Task>()
                        {
                            items = taskList.ToArray()
                        });
                    }
                    else
                    {
                        response = new Response<Wrapper<Task>>(resp.Error);
                    }

                    callback(response);
                });
        }

        public void CreateTask(string taskText, string gameField, string teacherId, Action<Response<Task>> callback)
        {
            var body = new Dictionary<string, string>
            {
                {"createdBy", teacherId},
                {"text", taskText},
                {"gameField", gameField},
            };

            var json = DictionaryToJson(body);
            Request<TaskWrapper>(Method.Post, Endpoint.TASKS, null, json, response =>
            {
                Response<Task> task;
                if (response.IsError)
                {
                    task = new Response<Task>(response.Error);
                }
                else
                {
                    TaskWrapper taskWrapper = response.Data;
                    task = new Response<Task>(new Task()
                    {
                        text = taskWrapper.text,
                        taskId = taskWrapper.taskId,
                        gameField = taskWrapper.gameField.Split('&'),
                    });
                }

                callback(task);
            });
        }

        public void GetSolutions(string teacherId, Action<Response<Wrapper<Decision>>> callback)
        {
            Request(Method.Get, $"{Endpoint.SOLUTIONS}?teacherId={teacherId}", null, null, callback);
        }

        public void SetGrade(int solutionId, int grade, Action<Response<Decision>> callback)
        {
            var body = new Dictionary<string, string>()
            {
                {"solutionId", solutionId.ToString()},
                {"grade", grade.ToString()},
            };
            var jsonBody = DictionaryToJson(body);
            Request(Method.Patch, Endpoint.SOLUTIONS, null, jsonBody, callback);
        }

        public void DeleteUser(string studentId, Action<Response<User>> callback)
        {
            Request(Method.Delete, $"{Endpoint.STUDENTS}?id={studentId}", null, null, callback);
        }

        public void SendSolution(Decision decision, Action<Response<Decision>> callback)
        {
            var body = new Dictionary<string, string>()
            {
                {"teacherId", decision.teacherId},
                {"taskId", decision.taskId},
                {"program", decision.program},
                {"createdBy", decision.createdBy},
            };
            var jsonBody = DictionaryToJson(body);
            Request(Method.Post, Endpoint.SOLUTIONS, null, jsonBody, callback);
        }

        private void Request<T>(Method method, string endpoint, Dictionary<string, object> headers, string body,
            Action<Response<T>> callback)
        {
            Debug.Log($"{BaseUrl}{endpoint}");
            var request = new UnityWebRequest($"{BaseUrl}{endpoint}")
            {
                method = method.ToString(),
                downloadHandler = new DownloadHandlerBuffer(),
            };

            ApplyBody(request, body);
            ApplyRequestHeader(request, headers);

            var response = request.SendWebRequest();
            response.completed += _ =>
            {
                Response<T> resp;
                if (request.isNetworkError || request.isHttpError)
                {
                    resp = new Response<T>(response.webRequest.responseCode);
                }
                else
                {
                    try
                    {
                        Debug.Log(request.downloadHandler.text);
                        var a = JsonUtility.FromJson<T>(request.downloadHandler.text);
                        resp = new Response<T>(a);
                    }
                    catch (Exception exc)
                    {
                        Debug.Log(exc.ToString());

                        Debug.Log(exc.Message);
                        resp = new Response<T>(-1);
                    }
                }

                callback(resp);
            };
        }

        private void ApplyRequestHeader(UnityWebRequest request, Dictionary<string, object> headers)
        {
            request.SetRequestHeader("Content-type", "application/json;  charset=utf-8");
            if (_username != null && _password != null)
            {
                var base64Credentials = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{_username}:{_password}"));
                request.SetRequestHeader("Authorization", "Basic " + base64Credentials);
            }

            if (headers == null) return;
            foreach (var key in headers.Keys)
            {
                request.SetRequestHeader(key, headers[key].ToString());
            }
        }

        private static void ApplyBody(UnityWebRequest request, string body)
        {
            if (body == null) return;
            var data = System.Text.Encoding.UTF8.GetBytes(body);
            request.uploadHandler = new UploadHandlerRaw(data);
        }

        private static string DictionaryToJson(Dictionary<string, string> dict)
        {
            var entries = dict.Select(d =>
                $"\"{d.Key}\": \"{d.Value}\"");
            return "{" + string.Join(", ", entries) + "}";
        }
    }

    public class Response<T>
    {
        private long error;
        private T data;

        public Response(long error)
        {
            this.error = error;
        }

        public Response(T data)
        {
            this.data = data;
        }

        public bool IsError => data == null;

        public T Data => data;

        public long Error => error;
    }

    internal static class Endpoint
    {
        public static string SIGN_UP = "/signup";
        public static string SIGN_IN = "/signin";
        public static string SOLUTIONS = "/solutions";
        public static string TASKS = "/tasks";
        public static string STUDENTS = "/students";
    }

    [Serializable]
    class TaskWrapper
    {
        public string text;
        public string gameField;
        public int taskId;
        public string createdBy;
    }
}