using System;
using System.Collections.Generic;
using Domain;

namespace Server
{
    public interface LSystemApi
    {
        void SignIn(string login, String password, Action<Response<User>> callback);

        void SignUp(string login, string name, string password, string role, Action<Response<User>> callback);

        void SignUpStudent(string login, string name, string password, string teacherId, string info,
            Action<Response<User>> callback);
        
        void EditStudent(string id, string login, string name, string password, string info,
            Action<Response<User>> callback);

        void GetStudents(string teacherId, Action<Response<Wrapper<User>>> callback);

        void GetTasks(string teacherId, Action<Response<Wrapper<Task>>> callback);

        void CreateTask(string taskText, string gameField, string teacherId, Action<Response<Task>> callback);

        void GetSolutions(string teacherId, Action<Response<Wrapper<Decision>>> callback);

        void SetGrade(int solutionId, int grade, Action<Response<Decision>> callback);

        void DeleteUser(string studentId, Action<Response<User>> callback);

        void SendSolution(Decision decision, Action<Response<Decision>> callback);
    }

    [Serializable]
    public class Wrapper<T>
    {
        public T[] items;
    }
}