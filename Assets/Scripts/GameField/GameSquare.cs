﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameSquare : MonoBehaviour, IGameSquare
{
    [SerializeField] private SpriteRenderer _sprite;
    public GameSquareNode SquareNode { get; private set; }
    public MovableObject movableObject;

    public SquareType SquareType
    {
        get
        {
            if (SquareNode != null)
                return SquareNode.type;
            return SquareType.Default;
        }
    }

    public MovableObjectType MovableObjectType
    {
        get
        {
            if (movableObject is Character)
                return MovableObjectType.Character;
            if (movableObject is Stone)
                return MovableObjectType.Stone;
            
            return MovableObjectType.None;
        }
    }

    private void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject() || GameManager.Instance.gamePanel.GamePanelMode != GamePanelMode.CreatingTask)
            return;
        GameManager.Instance.squareSettingsPanel.gameObject.SetActive(true);
        GameManager.Instance.squareSettingsPanel.Init(this);
    }


    public int PosI { get; private set; }
    public int PosJ { get; private set; }
    

    public void Init(GameSquareNode squareNode, int posI, int posJ)
    {
        SquareNode = squareNode;
        _sprite.sprite = squareNode.image;
        PosI = posI;
        PosJ = posJ;
    }
}


public enum SquareType
{   
    Default, Busy, Hole
}
