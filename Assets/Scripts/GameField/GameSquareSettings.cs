﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create Custom Object/Game Square Settings", fileName = "New Game Square")]
public class GameSquareSettings : ScriptableObject
{
    public List<GameSquareNode> nodes;
}

[System.Serializable]
public class GameSquareNode
{
    public Sprite image;
    public SquareType type;
    public string squareName;
    public string description;
    public string parseSymbols;
}
