public interface IGameSquare
{
    SquareType SquareType { get; }
    MovableObjectType MovableObjectType { get; }
}