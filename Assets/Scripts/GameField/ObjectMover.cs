﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMover : MonoBehaviour
{
    private GameField _gameField;
    private MovableJob _currentJob;

    private void Start()
    {
        _gameField = GameManager.Instance.gameField;
    }

    public Coroutine DoJob(MovableJob job)
    {
        _currentJob = job;
        if (isActiveAndEnabled)
            return StartCoroutine(DoJobRoutine(job));
        return null;
    }

    private IEnumerator DoJobRoutine(MovableJob job)
    {
        if (job.time <= 0)
        {
            transform.position = job.endSquare.transform.position;
            yield break;
        }

        var startPosition = job.startSquare.transform.position;
        var endPosition = job.endSquare.transform.position;
        
        yield return StartCoroutine(DoMove(startPosition, endPosition, job.time, job.mode));
        if (job.isDestroyingAfterJob)
        {
            GameManager.Instance.modalWindow.Init(ModalWindowMode.Error, "Муравей попал в лунку!", null);
            GameManager.Instance.modalWindow.gameObject.SetActive(true);
            Destroy(gameObject);
        }
    }
    

    private IEnumerator DoMove(Vector2 startPosition, Vector2 endPosition, float jobTime, RealProgressFuncMode mode)
    {
        transform.position = startPosition;
        var progress = 0f;
        var progressInSecond = 1 / (jobTime / GameManager.Instance.timeCoefficient);
        //progress += progressInSecond * Time.deltaTime;
        var realProgress = progress;
        while (progress < 1f)
        {
            if (mode == RealProgressFuncMode.Default)
                realProgress = progress;
            else
                realProgress = Mathf.Sqrt(progress);
            
            transform.position = Vector2.Lerp(startPosition, endPosition, realProgress);
            yield return null;
            progress += progressInSecond * Time.deltaTime;
        }

        transform.position = endPosition;
        _currentJob = null;
        yield break;
    }

    public void StopMove()
    {
        StopAllCoroutines();
        if (_currentJob != null)
            transform.position = _currentJob.startSquare.transform.position;
        _currentJob = null;
    }
}

public class MovableJob
{
    public GameSquare startSquare;
    public GameSquare endSquare;
    public RealProgressFuncMode mode;
    public float time;
    public bool isDestroyingAfterJob;

    public MovableJob(GameSquare startSquare, GameSquare endSquare, float time, RealProgressFuncMode mode, bool isDestroyingAfterJob = false)
    {
        this.startSquare = startSquare;
        this.endSquare = endSquare;
        this.time = time;
        this.mode = mode;
        this.isDestroyingAfterJob = isDestroyingAfterJob;
    }
}

public enum RealProgressFuncMode
{
    Default, Jump
}
