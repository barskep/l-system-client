﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Character : MovableObject
{
    public void Start()
     {
         GameManager.Instance.character = this;
     }
    
 
     protected override Coroutine CreateJob(DirectionInfo info)
     {
         var newI = CurrentSquare.PosI;
         var newJ = CurrentSquare.PosJ;
 
         if (info.direction == Direction.Down)
             newI += info.steps;
         if (info.direction == Direction.Up)
             newI -= info.steps;
         if (info.direction == Direction.Left)
             newJ -= info.steps;
         if (info.direction == Direction.Right)
             newJ += info.steps;
 
         if (gameField[newI, newJ] == null || (info.steps > 1 && gameField[newI, newJ].movableObject != null))
             if (GameManager.Instance.OnGameFieldError != null)
             {
                 GameManager.Instance.OnGameFieldError();
                 return null;
             }

         if (gameField[newI, newJ].movableObject != null)
             gameField[newI, newJ].movableObject.AddDirectionInfoToQueue(info);
         
         var job = new MovableJob(CurrentSquare, gameField[newI, newJ],
             defaultTime * info.steps, info.steps > 1 ? RealProgressFuncMode.Jump : RealProgressFuncMode.Default,
             gameField[newI,newJ].SquareType == SquareType.Hole);
         
         CurrentSquare.movableObject = null;
         CurrentSquare = gameField[newI, newJ];

         return _mover.DoJob(job);
     }
}


