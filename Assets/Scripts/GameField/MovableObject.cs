﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ObjectMover))]
public abstract class MovableObject : MonoBehaviour
{
    [SerializeField] protected ObjectMover _mover; 
    [SerializeField] protected float defaultTime = 1f; 
    protected readonly Queue<DirectionInfo> directionInfos = new Queue<DirectionInfo>(); 
    protected GameField gameField; 
    public GameSquare CurrentSquare { get; protected set; }
    
    
    public virtual void Init(GameSquare startSquare)
    {
        CurrentSquare = startSquare;
        var position = CurrentSquare.transform.position;
        transform.position = new Vector3(position.x, position.y, -1);
        startSquare.movableObject = this;
        
        gameField = GameManager.Instance.gameField;
        GameManager.Instance.OnGameFieldError += OnError;
        StartCoroutine(CheckQueue());
    }

    private IEnumerator CheckQueue()
    {
        while (true)
        {
            if (directionInfos.Count > 0)
            {
                var info = directionInfos.Dequeue();
                yield return CreateJob(info);
                CurrentSquare.movableObject = this;
            }
            yield return null;
        }
    }
    protected abstract Coroutine CreateJob(DirectionInfo info);

    public void AddDirectionInfoToQueue(DirectionInfo info)
    {
        directionInfos.Enqueue(info);
    }

    public void OnError()
    {
        if (isActiveAndEnabled)
        {
            StopAllCoroutines();
            directionInfos.Clear();
            _mover.StopMove();
        }
    }

    public void OnDestroy()
    {
        GameManager.Instance.OnGameFieldError -= OnError;
    }
}

public enum Direction
{
    Up, Down, Left, Right
}

public struct DirectionInfo
{
    public Direction direction;
    public int steps;

    public DirectionInfo(Direction direction, int steps)
    {
        this.direction = direction;
        this.steps = steps;
    }
}
