﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create Custom Object/Field Object Settings", fileName = "FieldObjectSettings")]
public class FieldObjectSettings : ScriptableObject
{
    public List<MovableObjectNode> nodes;
}

[System.Serializable]
public class MovableObjectNode
{
    public MovableObjectType type;
    public MovableObject prefab;
    public string name;
    public string description;
    public string parseSymbols;
}

public enum MovableObjectType
{
    Character, Stone, None
}
