﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Security.Cryptography;
using TMPro.EditorUtilities;
using UnityEngine;

public class GameField : MonoBehaviour
{
    private const int POOL_SIZE = 225;
    private static readonly Vector2 DISABLED_POSITION = new Vector2(100, 100);
    [SerializeField] private Vector2 _defaultPosition;
    [SerializeField] private Vector2 _offset;
    [SerializeField] private float _sizeCoefficient;
    [SerializeField] private GameSquareSettings _gameSquareSettings;
    [SerializeField] private FieldObjectSettings _fieldObjectSettings;
    [SerializeField] private GameSquare _prefab;
    private string[] _fieldInfo;
    private Queue<GameSquare> _squaresPool;
    private List<MovableObject> _movableObjects;
    private GameSquare[,] _squares;
    public IGameSquare[,] Squares => _squares;

    private void Awake()
    {
        _squaresPool = new Queue<GameSquare>();
        for (int i = 0; i < POOL_SIZE; i++)
        {
            var square = Instantiate(_prefab, DISABLED_POSITION,  Quaternion.identity, transform);
            square.gameObject.SetActive(false);
            _squaresPool.Enqueue(square);
        }
    }

    public void CreateField(string[] fieldInfo)
    {
        _fieldInfo = fieldInfo;
        ClearField();
        if (fieldInfo == null || string.IsNullOrEmpty(fieldInfo[0]))
            throw new ArgumentOutOfRangeException();
        
        var fieldSymbols = new string[fieldInfo.Length][];
        fieldSymbols[0] = fieldInfo[0].Split(' ');
        
        if (fieldSymbols[0].Length <= 0)
            throw new ArgumentOutOfRangeException(); 
        for (var i = 1; i < fieldInfo.Length; i++)
        {
            fieldSymbols[i] = fieldInfo[i].Split(' ');
            if (fieldSymbols[i].Length != fieldSymbols[0].Length)
                throw new ArgumentOutOfRangeException();
        }
        
        var gameSquareNodes = new GameSquareNode[fieldSymbols.Length, fieldSymbols[0].Length];
        var movableObjectNodes = new MovableObjectNode[fieldSymbols.Length, fieldSymbols[0].Length];

        for (var i = 0; i < gameSquareNodes.GetLength(0); i++)
        {
            for (var j = 0; j < gameSquareNodes.GetLength(1); j++)
            {
                var symbols = fieldSymbols[i][j].Split(':');
                
                gameSquareNodes[i,j] = _gameSquareSettings.nodes.Find(t => symbols[0] == t.parseSymbols);
                if (gameSquareNodes[i,j] == null)
                    throw new Exception("Unknown Symbols!" + fieldSymbols[i][j] + ":::");

                if (symbols.Length <= 1) continue;
                
                movableObjectNodes[i, j] = _fieldObjectSettings.nodes.Find(t => symbols[1] == t.parseSymbols);
                if (movableObjectNodes[i,j] == null)
                    throw new Exception("Unknown Symbols!");

            }
        }
        InstantiateGameSquares(gameSquareNodes);
        InstantiateMovableObjects(movableObjectNodes);
        SetFieldScale();
    }

    public void RestartLevel()
    {
        CreateField(_fieldInfo);
    }

    private void InstantiateGameSquares(GameSquareNode[,] nodes)
    {
        _squares = new GameSquare[nodes.GetLength(0), nodes.GetLength(1)];
        var squaresLength0 = nodes.GetLength(0);
        var squaresLength1 = nodes.GetLength(1);
        var position = transform.position;
        for (var i = 0; i < squaresLength0; i++)
        {
            for (var j = 0; j < squaresLength1; j++)
            {
                _squares[i, j] = GetPrefabFromPool();
                var iOffsetToCenter = (-(squaresLength0 - 1) / 2f + i) * _offset.y;
                var jOffsetToCenter = (-(squaresLength1 - 1) / 2f + j) * _offset.x;
                
                _squares[i, j].transform.position = new Vector2(position.x + jOffsetToCenter, 
                    position.y + iOffsetToCenter);
                _squares[i, j].Init(nodes[i,j], i, j);
            }
        }
    }

    private void InstantiateMovableObjects(MovableObjectNode[,] nodes)
    {
        _movableObjects = new List<MovableObject>();
        for (var i = 0; i < nodes.GetLength(0); i++)
        {
            for (var j = 0; j < nodes.GetLength((1)); j++)
            {
                if (nodes[i, j] != null)
                {
                    var movableObject = Instantiate(nodes[i, j].prefab, transform);
                    movableObject.Init(_squares[i,j]);
                    _movableObjects.Add(movableObject);
                }
            }
        }
    }

    public Stone InstantiateStone(int posI, int posJ)
    {
        var stoneSettings = _fieldObjectSettings.nodes.Find(t => t.type == MovableObjectType.Stone);
        var stone = Instantiate(stoneSettings.prefab, _squares[posI, posJ].transform.parent);
        stone.Init(_squares[posI, posJ]);
        _movableObjects.Add(stone);
        //stone.transform.localScale = stone.transform.localScale.x * transform.localScale;
        return stone as Stone;
    }

    public void MoveCharacter(int posI, int posJ)
    {
        var character = GameManager.Instance.character;
        if (character == null)
        {
            var characterSettings = _fieldObjectSettings.nodes.Find(t => t.type == MovableObjectType.Character);
            character = Instantiate(characterSettings.prefab, _squares[posI, posJ].transform);
            character.Init(_squares[posI, posJ]);
            GameManager.Instance.character = character;
            _movableObjects.Add(character);
            character.transform.localScale = character.transform.localScale.x * transform.localScale;
        }

        character.CurrentSquare.movableObject = null;
        character.Init(_squares[posI, posJ]);
    }

    private void SetFieldScale()
    {
        var squaresLength0 = _squares.GetLength(0);
        var squaresLength1 = _squares.GetLength(1);
        
        var newScale = squaresLength0 > squaresLength1
            ? 1f / squaresLength0 : 1f / squaresLength1;

        transform.localScale = new Vector3(newScale, newScale, 1) * _sizeCoefficient;
        transform.position = _defaultPosition;
    }

    public GameSquare this [int i, int j]
    {
        get
        {
            if (i < 0 || j < 0 || i >= _squares.GetLength(0) || j >= _squares.GetLength(1))
                return null;
            return _squares[i, j];
        }
    }

    private GameSquare GetPrefabFromPool()
    {
        if (_squaresPool != null && _squaresPool.Count > 0)
        {
            var square = _squaresPool.Dequeue();
            square.gameObject.SetActive(true);
            return square;
        }

        return Instantiate(_prefab, DISABLED_POSITION, Quaternion.identity, transform);
    }

    public void DestroyMovable(MovableObject movable)
    {
        if (_movableObjects.Contains(movable))
            _movableObjects.Remove(movable);

        Destroy(movable.gameObject);
    }

    private void ReturnSquareToPool(GameSquare square)
    {
        square.transform.position = DISABLED_POSITION;
        square.transform.parent = transform;
        square.gameObject.SetActive(false);
    }

    public void ClearField()
    {
        if (_squares != null)
            foreach (var square in _squares)
                ReturnSquareToPool(square);

        if (_movableObjects != null)
        {
            foreach (var movable in _movableObjects)
                if (movable != null)
                    Destroy(movable.gameObject);
            _movableObjects.Clear();
        }
        
        transform.localScale = new Vector3(1, 1, 1);
    }

    public void UpdateField()
    {
        _fieldInfo = GameManager.Instance.gamePanel.GetGameField(_squares);
    }
}
