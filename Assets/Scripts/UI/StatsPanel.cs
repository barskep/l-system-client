﻿using System.Collections;
using System.Collections.Generic;
using Domain;
using UnityEngine;

public class StatsPanel : MonoBehaviour
{
    private List<Decision> _decisions;

    [SerializeField] private ResultScroll _resultScroll;
    [SerializeField] private ResultGraph _resultGraph;
    
    public void Init(List<Decision> decisions, List<User> users)
    {
        _decisions = decisions;
        _resultScroll.Init(decisions, users);
        _resultGraph.Init(decisions);
        OnClickGraph();
    }

    public void OnClickGraph()
    {
        _resultScroll.gameObject.SetActive(false);
        _resultGraph.gameObject.SetActive(true);
    }

    public void OnClickScroll()
    {
        _resultScroll.gameObject.SetActive(true);
        _resultGraph.gameObject.SetActive(false);
    }

    public void OnClickClose()
    {
        gameObject.SetActive(false);
    }
}
