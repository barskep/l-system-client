﻿using System.Collections;
using System.Collections.Generic;
using Domain;
using UnityEngine;
using UnityEngine.UI;

public class MarkPanel : MonoBehaviour
{
    [SerializeField] private Dropdown _dropDown;

    public void OnClickClose()
    {
        gameObject.SetActive(false);
    }

    public void SetValue(int mark)
    {
        _dropDown.value = mark >= 2 && mark <= 5 ? mark - 2 : 2;
    }

    public void OnClickEstimate()
    {
        var result = new Result()
        {
            mark = (_dropDown.value + 2).ToString(),
            pupilId = GameManager.Instance.currentDecision.createdBy,
            teacherId = GameManager.Instance.currentDecision.teacherId,
            taskId = GameManager.Instance.currentDecision.taskId
        };
        GameManager.Instance.OpenLoader();
        GameManager.Instance.serverApi.SetGrade(GameManager.Instance.currentDecision.solutionId, (_dropDown.value + 2),
            response =>
            {
                if (!response.IsError)
                {
                    GameManager.Instance.CloseLoaderWithDelay(() =>
                    {
                        GameManager.Instance.teacherMenuPanel.gameObject.SetActive(true);
                        gameObject.SetActive(false);
                    });
                }
            });
        //TODO: Added OnclickClose() after upload result on server
    }
}