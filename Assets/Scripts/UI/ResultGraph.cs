﻿using System.Collections;
using System.Collections.Generic;
using Domain;
using UnityEngine;
using UnityEngine.UI;

public class ResultGraph : MonoBehaviour
{
    private List<Decision> _decisions;
    [SerializeField] private Image[] _markImages;
    [SerializeField] private Text[] _markCounts;
    [SerializeField] private Dictionary<int, List<Decision>> _markedResults;

    public void Init(List<Decision> decisions)
    {
        _decisions = decisions;
        var resultsCount = _decisions.Count > 0 ? _decisions.Count : 1;
        _markedResults = new Dictionary<int, List<Decision>>();
        for (int i = 0; i < 4; i++)
        {
            var list = _decisions.FindAll(t => t.grade == i + 2);
            _markedResults.Add(i + 2, _decisions.FindAll(t => t.grade == i + 2));
            if (_markedResults[i + 2] != null && _markedResults[i+2].Count > 0)
            {
                _markImages[i].fillAmount = _markedResults.Count / (float) resultsCount;
                _markCounts[i].text = _markedResults[i+2].Count.ToString();
            }
            else
            {
                _markImages[i].fillAmount = 0;
                _markCounts[i].text = "0";
            }
        }
    }
}
