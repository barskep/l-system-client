﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Threading;
using UnityEngine.UI;

public class Cell : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Command currentCommand;
    [SerializeField] private Image _sprite;
    [SerializeField] private Color _highLightedColor;
    [SerializeField] private Color _unHighLightedColor;
    [SerializeField] private Sprite _defaultSprite;
    private bool inAlgorithm;

    public bool InAlgorithm
    {
        get => inAlgorithm;
        set
        {
            inAlgorithm = value;
            if (currentCommand != null)
                currentCommand.InAlgorithm = value;
        }
    }

    public Command CurrentCommand
    {
        get => currentCommand;
        set
        {
            currentCommand = value;
            if (currentCommand != null)
            {
                _sprite.sprite = currentCommand.Node.sprite;
                currentCommand.transform.position = transform.position;
            }
            else _sprite.sprite = _defaultSprite; 

            if (GameManager.Instance.OnCommandsUpdate != null)
                GameManager.Instance.OnCommandsUpdate();
        }
    }

    public string GetCommandCode()
    {
        return CurrentCommand != null ? CurrentCommand.GenerateCommandCode() : "";
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (currentCommand != null)
            return;
        
        if (GameManager.Instance.dragElements.ContainsKey(eventData.pointerDrag))
        {
            var commandDrag = GameManager.Instance.dragElements[eventData.pointerDrag];
            var command =  Instantiate(commandDrag.spawnedPrefab, transform);
            command.Init(commandDrag.node, this, inAlgorithm);
            CurrentCommand = command;
        }
        else if (GameManager.Instance.commands.ContainsKey(eventData.pointerDrag))
        {
            var command = GameManager.Instance.commands[eventData.pointerDrag];
            command.transform.SetParent(transform);
            command.Init(command.Node, this, inAlgorithm);
            CurrentCommand = command;
        }
    }

    public Vector3 GetBottomCellPosition()
    {
        if (CurrentCommand == null)
            return transform.position;

        Vector3 subCommandBottom = transform.position;
        Vector3 nextCommandBottom = transform.position;
        if (CurrentCommand.SubCommand != null)
            subCommandBottom = CurrentCommand.SubCommand.GetBottomCellPosition();

        if (CurrentCommand.NextCommand != null)
            nextCommandBottom = CurrentCommand.NextCommand.GetBottomCellPosition();

        return (subCommandBottom.y < nextCommandBottom.y) ? subCommandBottom : nextCommandBottom;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null || 
            (!GameManager.Instance.dragElements.ContainsKey(eventData.pointerDrag) && !GameManager.Instance.commands.ContainsKey(eventData.pointerDrag))
            || currentCommand != null)
            return;

        _sprite.color = _highLightedColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _sprite.color = _unHighLightedColor;
    }
}
