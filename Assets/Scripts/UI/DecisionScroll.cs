﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain;
using UnityEditor;
using UnityEngine;

public class DecisionScroll : MonoBehaviour
{
    [SerializeField] private DecisionItem _prefab;
    [SerializeField] private Transform _parent;
    private List<DecisionItem> _items;
    
    

    public void Init( List<Decision> decisions, List<Task> tasks)
    {
        _prefab.gameObject.SetActive(true);
        if (_items == null)
            _items = new List<DecisionItem>();
        foreach (var item in _items)
        {
            Destroy(item.gameObject);
        }
        _items.Clear();
        foreach (var decision in decisions)
        {
            var item = Instantiate(_prefab, _parent);
            _items.Add(item);
            item.Init(decision, tasks.Find(t => t.taskId == int.Parse(decision.taskId)));
        }

        _prefab.gameObject.SetActive(false);
    }
}
