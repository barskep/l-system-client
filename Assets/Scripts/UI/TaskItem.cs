﻿using System.Collections;
using System.Collections.Generic;
using Domain;
using UnityEngine;
using UnityEngine.UI;

public class TaskItem : MonoBehaviour
{
    [SerializeField] private Text _name;
    private Task _currentTask;

    public void Init(Task task)
    {
        _currentTask = task;
        _name.text = "Задание " + task.taskId;
    }

    public void OnClickTask()
    {
        GameManager.Instance.OpenTask(_currentTask);
    }
}
