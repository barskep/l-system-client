﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CommandDragElement : DragElement
{
    [SerializeField] protected TextMeshProUGUI _commandText;
    [SerializeField] protected Image _sprite;
    private Command _spawnedPrefab;

    public Command spawnedPrefab => _spawnedPrefab;

    private CommandNode _node;

    public CommandNode node => _node;

    public virtual void Init(Transform idleParent, Transform dragParent, CommandNode node)
    {
        base.Init(idleParent, dragParent);
        _node = node;
        _spawnedPrefab = node.prefab;
        _commandText.text = node.commandName;
        GameManager.Instance.dragElements.Add(gameObject, this);
        _sprite.sprite = node.sprite;
        _sprite.color = node.color;
    }

}
