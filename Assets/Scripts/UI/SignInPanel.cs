﻿using System;
using System.Collections;
using Server;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class SignInPanel : MonoBehaviour
{
    [SerializeField] private InputField _loginField;
    [SerializeField] private InputField _passField;

    public void OnClickSignIn()
    {
        GameManager.Instance.OpenLoader();
        GameManager.Instance.serverApi.SignIn(_loginField.text, _passField.text, OnSignIn);
    }

    private void OnSignIn(Response<User> userResponse)
    {
        if (!userResponse.IsError)
        {
            Debug.Log(
                $"Login success. User name is {userResponse.Data.name}, teacherId is {userResponse.Data.teacherId}");
            if (userResponse.Data.IsStudent())
            {
                GameManager.Instance.pupilMenuPanel.Init(userResponse.Data);
                GameManager.Instance.pupilMenuPanel.gameObject.SetActive(true);
                GameManager.Instance.CloseLoaderWithDelay(null);
            }
            else
            {
                GameManager.Instance.teacherMenuPanel.SetTeacher(userResponse.Data);
                GameManager.Instance.teacherMenuPanel.gameObject.SetActive(true);
                GameManager.Instance.teacherMenuPanel.StartUpdate();
            }
            gameObject.SetActive(false);

        }
        else
        {
            GameManager.Instance.CloseLoaderImmediately(null);
            GameManager.Instance.modalWindow.Init(ModalWindowMode.Error, $"Произошла ошибка. {userResponse.Error}",
                null);
            GameManager.Instance.modalWindow.gameObject.SetActive(true);
        }
    }

    public void OnClickClose()
    {
        Application.Quit();
    }

    public void OnClickRegister()
    {
        GameManager.Instance.registerPanel.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}

[Serializable]
public class User
{
    public string id;
    public string username;
    public string password;
    public string name;
    public string role;
    public string info;
    public string teacherId;

    public bool IsStudent()
    {
        return role == "student";
    }
}

public enum UserType
{
    Teacher,
    Pupil
}