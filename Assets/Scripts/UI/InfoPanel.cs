﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InfoPanel : MonoBehaviour
{
    [SerializeField] private string _aboutDevelopersURL;
    [SerializeField] private GameObject _rules;
    [SerializeField] private GameObject _pupilMode;
    [SerializeField] private GameObject _teacherMode;

    public void Start()
    {
        OnClickRules();
    }

    public void OnClickRules()
    {
        _rules.gameObject.SetActive(true);
        _pupilMode.gameObject.SetActive(false);
        _teacherMode.gameObject.SetActive(false);
    }
    
    public void OnClickPupilMode()
    {
        _rules.gameObject.SetActive(false);
        _pupilMode.gameObject.SetActive(true);
        _teacherMode.gameObject.SetActive(false);
    }
    
    public void OnClickTeacherMode()
    {
        _rules.gameObject.SetActive(false);
        _pupilMode.gameObject.SetActive(false);
        _teacherMode.gameObject.SetActive(true);
    }

    public void OnClickAboutDevelopers()
    {
        Application.OpenURL(_aboutDevelopersURL);
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }
}
