﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using Domain;
using UnityEngine;
using UnityEngine.UI;

public class DecisionItem : MonoBehaviour
{
    [SerializeField] private Text _name;
    private Decision _currentDecision;
    private Task _currentTask;

    public void Init(Decision decision, Task task)
    {
        _currentTask = task;
        _currentDecision = decision;
        _name.text =  "Решение к задаче №" + task.taskId;
    }

    public void OnClickTask()
    {
        GameManager.Instance.CheckDecision(_currentDecision, _currentTask);
        GameManager.Instance.teacherMenuPanel.gameObject.SetActive(false);
        GameManager.Instance.pupilInfoPanel.gameObject.SetActive(false);
        GameManager.Instance.decisionsPanel.gameObject.SetActive(false);
    }
}
