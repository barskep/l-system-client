﻿using System.Collections;
using System.Collections.Generic;
using Domain;
using UnityEngine;

public class ResultScroll : MonoBehaviour
{
    [SerializeField] private ResultItem _prefab;
    [SerializeField] private Transform _parent;
    private List<Decision> _decisions;
    private List<ResultItem> _items;

    public void Init(List<Decision> decisions, List<User> users)
    {
        _prefab.gameObject.SetActive(true);
        _decisions = decisions;
        if (_items == null)
            _items = new List<ResultItem>();

        foreach (var item in _items)
        {
            Destroy(item.gameObject);
        }
        _items.Clear();
        foreach (var decision in decisions)
        {
            var item = Instantiate(_prefab, _parent);
            _items.Add(item);
            item.Init(decision, users);
        }

        _prefab.gameObject.SetActive(false);
    }
}
