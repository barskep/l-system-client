﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class DragElement : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField, HideInInspector] protected Transform _idleParent;
    [SerializeField, HideInInspector] protected Transform _dragParent;
    [SerializeField] protected CanvasGroup _canvasGroup;
    protected int _siblingIndex;
    protected Vector3 _offset;

    protected virtual void Init(Transform idleParent, Transform dragParent)
    {
        _idleParent = idleParent;
        _dragParent = dragParent;
    }

    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        _offset = transform.position - Camera.main.ScreenToWorldPoint(eventData.position);
        _siblingIndex = transform.GetSiblingIndex();
        transform.SetParent(_dragParent);
        _canvasGroup.blocksRaycasts = false;
    }
    
    public virtual void OnDrag(PointerEventData eventData)
    {
        var eventPos = Camera.main.ScreenToWorldPoint(eventData.position);
        var newPos = new Vector3(eventPos.x, eventPos.y, 1);
        transform.position = newPos + _offset;
    }

    public virtual void OnEndDrag(PointerEventData eventData)
    {
        transform.SetParent(_idleParent);
        transform.position = transform.parent.position;
        transform.SetSiblingIndex(_siblingIndex);
        _canvasGroup.blocksRaycasts = true;
    }


}
