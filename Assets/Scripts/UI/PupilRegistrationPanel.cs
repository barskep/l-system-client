﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Server;
using UnityEngine;
using UnityEngine.UI;

public class PupilRegistrationPanel : RegisterPanel
{
    [SerializeField] protected InputField _infoText;

    public override void OnClickRegister()
    {
        var user = new User()
        {
            username = _loginField.text,
            password = _passField.text,
            name = _nameField.text,
            info = _infoText.text
        };
        
        if (user.username.Length == 0 || user.password.Length == 0 || user.name.Length == 0)
        {
            GameManager.Instance.modalWindow.Init(ModalWindowMode.Error, $"Ни одно из полей не может быть пустым. ",
                null);
            GameManager.Instance.modalWindow.gameObject.SetActive(true);
        }
        else
        {
            if (user.name.All((character) => character == ' ' || (character >= 'А' && character <= 'Я') ||
                                        (character >= 'а' && character <= 'я') || character == '-'))
            {
                
                GameManager.Instance.OpenLoader();
                GameManager.Instance.serverApi.SignUpStudent(_loginField.text, _nameField.text, _passField.text,
                    GameManager.Instance.teacherMenuPanel.teacher.id, _infoText.text,
                    response =>
                    {
                        if (!response.IsError)
                        {
                            GameManager.Instance.CloseLoaderWithDelay(() =>
                            {
                                var teacherPanel = GameManager.Instance.teacherMenuPanel;
                                teacherPanel.gameObject.SetActive(true);
                                teacherPanel.pupils.Add(response.Data);
                                teacherPanel.Init(teacherPanel.teacher, teacherPanel.pupils);
                                Clear();
                                gameObject.SetActive(false);
                            });
                        }
                        else
                        {
                            GameManager.Instance.CloseLoaderImmediately(null);
                            GameManager.Instance.modalWindow.Init(ModalWindowMode.Error, $"Произошла ошибка. {response.Error}", null);
                            GameManager.Instance.modalWindow.gameObject.SetActive(true);

                        }
                    });
            }
            else
            {
                GameManager.Instance.modalWindow.Init(ModalWindowMode.Error, $"Имя может содержать только руские буквы, пробел и дефис",
                    null);
                GameManager.Instance.modalWindow.gameObject.SetActive(true);
            }
        }

    }

    protected override void OnRegister(Response<User> userResponse)
    {
        GameManager.Instance.teacherMenuPanel.gameObject.SetActive(true);
        Clear();
        gameObject.SetActive(false);
    }

    public void Close()
    {
        GameManager.Instance.teacherMenuPanel.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }

    public void Clear()
    {
        _loginField.text = "";
        _infoText.text = "";
        _nameField.text = "";
        _passField.text = "";
    }
}