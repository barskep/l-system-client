﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

public class GameFieldSettingsPanel : MonoBehaviour
{
    private const int POOL_SIZE = 225;
    private const string ANIMATOR_TRIGGER_OPEN = "Open";
    private const string ANIMATOR_TRIGGER_CLOSE = "Close";
    [SerializeField] private GameSquareSettings _gameSquareSettings;
    [SerializeField] private FieldObjectSettings _fieldObjectSettings;
    [SerializeField] private Slider _stonesSlider;
    [SerializeField] private Slider _holesSlider;
    [SerializeField] private Slider _busiesSlider;
    [SerializeField] private InputField _xField;
    [SerializeField] private InputField _yField;
    [SerializeField] private InputField _taskText;
    [SerializeField] private Animator _animator;
    [SerializeField] private Text _animationButtonText;
    private Queue<VirtualGameSquare> _virtualPool;
    private VirtualGameSquare[,] _squares;
    private int x;
    private int y;
    private bool _isOpen;
    public string TaskText => _taskText.text;

    public void Start()
    {
        _isOpen = false;
        _virtualPool = new Queue<VirtualGameSquare>();
        for (int i = 0; i < POOL_SIZE; i++)
            _virtualPool.Enqueue(new VirtualGameSquare());

        _xField.text = "3";
        _yField.text = "3";
    }

    public void OnClickGenerate()
    {
        x = int.Parse(_xField.text);
        y = int.Parse(_yField.text);
        
        if (_squares != null)
            foreach (var square in _squares)
                _virtualPool.Enqueue(square);
        
        _squares = new VirtualGameSquare[x,y];
        
        for (int i = 0; i < x; i++)
            for (int j = 0; j < y; j++)
            {
                _squares[i, j] = GetSquareFromPool();
                _squares[i, j].squareType = SquareType.Default;
                _squares[i, j].movableObjectType = MovableObjectType.None;
            }

        RandomizeCharacter();
        RandomizeStones();
        RandomizeHoles();
        RandomizeBusies();
        
        GameManager.Instance.gameField.CreateField(GetGameField(_squares));
    }

    public void OnClickAnimateButton()
    {
        _isOpen = !_isOpen;
        _animator.SetTrigger(_isOpen ? ANIMATOR_TRIGGER_OPEN : ANIMATOR_TRIGGER_CLOSE);
    }

    public void ChangeButtonTextCallback()
    {
        _animationButtonText.text = _isOpen ? ">" : "<";
    }

    private void RandomizeStones()
    {
        var stonesCount = Mathf.RoundToInt(_stonesSlider.value / 100f * x * y);

        for (int i = 0; i < 10000 && stonesCount > 0; i++)
        {
            var vector = IntVector.GetRandomVector(0, x, 0, y);
            var randSquare = _squares[vector.x, vector.y];
            if (randSquare.squareType == SquareType.Default && randSquare.movableObjectType == MovableObjectType.None)
            {
                randSquare.movableObjectType = MovableObjectType.Stone;
                stonesCount--;
            }
        }

        if (stonesCount > 0)
            Debug.LogError("Генерация камней прошла с ошибкой, стек операций был переполнен!");
    }

    private void RandomizeHoles()
    {
        var holesCount = Mathf.RoundToInt(_holesSlider.value / 100f * x * y);

        for (int i = 0; i < 10000 && holesCount > 0; i++)
        {
            var vector = IntVector.GetRandomVector(0, x, 0, y);
            var randSquare = _squares[vector.x, vector.y];
            if (randSquare.squareType == SquareType.Default && randSquare.movableObjectType == MovableObjectType.None)
            {
                randSquare.squareType = SquareType.Hole;
                holesCount--;
            }
        }

        if (holesCount > 0)
            Debug.LogError("Генерация лунок прошла с ошибкой, стек операций был переполнен!");
    }

    private void RandomizeBusies()
    {
        var busiesCount = Mathf.RoundToInt(_busiesSlider.value / 100f * x * y);

        for (int i = 0; i < 10000 && busiesCount > 0; i++)
        {
            var vector = IntVector.GetRandomVector(0, x, 0, y);
            var randSquare = _squares[vector.x, vector.y];
            if (randSquare.squareType == SquareType.Default && randSquare.movableObjectType == MovableObjectType.None)
            {
                randSquare.squareType = SquareType.Busy;
                busiesCount--;
            }
        }

        if (busiesCount > 0)
            Debug.LogError("Генерация занятых клеток прошла с ошибкой, стек операций был переполнен!");
    }

    private void RandomizeCharacter()
    {
        for (int i = 0; i < 10000; i++)
        {
            var vector = IntVector.GetRandomVector(0, x, 0, y);
            var randSquare = _squares[vector.x, vector.y];
            if (randSquare.squareType == SquareType.Default && randSquare.movableObjectType == MovableObjectType.None)
            {
                randSquare.movableObjectType = MovableObjectType.Character;
                return;
            }
        }
        
        Debug.LogError("Генерация положения персонажа произошла с ошибкой, стек операций переполнен!");
    }
    private VirtualGameSquare GetSquareFromPool()
    {
        if (_virtualPool != null && _virtualPool.Count > 0)
            return _virtualPool.Dequeue();

        return new VirtualGameSquare();
    }

    public string[] GetGameField(IGameSquare[,] squares)
    {
        string[] gameField = new string[y];
        for (int j = 0; j < x; j++)
        {
            gameField[j] = "";
            for (int i = 0; i < y; i++)
            {
                gameField[j] += GetSquaresParseString(squares[j, i]) + " ";
            }

            gameField[j] = gameField[j].Trim();
        }

        return gameField;
    }

    private string GetSquaresParseString(IGameSquare square)
    {
        var result = _gameSquareSettings.nodes.Find(t => t.type == square.SquareType).parseSymbols;
        if (square.MovableObjectType == MovableObjectType.None)
            return result;

        result = result + ":" +
                     _fieldObjectSettings.nodes.Find(t => t.type == square.MovableObjectType).parseSymbols;
        return result;
    }
}

public class VirtualGameSquare : IGameSquare
{
    public SquareType squareType;
    public MovableObjectType movableObjectType;
    public SquareType SquareType => squareType;
    public MovableObjectType MovableObjectType => movableObjectType;
}

public struct IntVector
{
    public int x;
    public int y;

    public static IntVector GetRandomVector(int minX, int maxX, int minY, int maxY)
    {
        var vec = new IntVector
        {
            x = Mathf.RoundToInt(UnityEngine.Random.Range(minX, maxX)),
            y = Mathf.RoundToInt(UnityEngine.Random.Range(minY, maxY))
        };
        return vec;
    }
}
