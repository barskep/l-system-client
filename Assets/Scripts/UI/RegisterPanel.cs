﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Server;
using UnityEngine;
using UnityEngine.UI;

public class RegisterPanel : MonoBehaviour
{
    [SerializeField] protected InputField _loginField;
    [SerializeField] protected InputField _passField;
    [SerializeField] protected InputField _nameField;


    public virtual void OnClickRegister()
    {
        var user = new User()
        {
            username = _loginField.text,
            password = _passField.text,
            name = _nameField.text
        };

        if (user.username.Length == 0 || user.password.Length == 0 || user.name.Length == 0)
        {
            GameManager.Instance.modalWindow.Init(ModalWindowMode.Error, $"Поля логина, пароля и ФИО не могут быть пустыми. ",
                null);
            GameManager.Instance.modalWindow.gameObject.SetActive(true);
        }
        else
        {
            if (user.name.All((character) => character == ' ' || (character >= 'А' && character <= 'Я') ||
                                        (character >= 'а' && character <= 'я') || character == '-'))
            {
                GameManager.Instance.OpenLoader();
                GameManager.Instance.serverApi.SignUp(_loginField.text, _nameField.text, _passField.text, "teacher",
                    OnRegister);
            }
            else
            {
                GameManager.Instance.modalWindow.Init(ModalWindowMode.Error, $"Имя может содержать только руские буквы, пробел и дефис",
                    null);
                GameManager.Instance.modalWindow.gameObject.SetActive(true);
            }
        }


    }

    protected virtual void OnRegister(Response<User> userResponse)
    {
        GameManager.Instance.CloseLoaderWithDelay(() =>
        {
            if (!userResponse.IsError)
            {
                Debug.Log($"Registration success. User name is {userResponse.Data.name}");
                GameManager.Instance.gamePanel.gameObject.SetActive(false);
                GameManager.Instance.signInPanel.gameObject.SetActive(true);
                gameObject.SetActive(false);
            }
            else
            {
                GameManager.Instance.CloseLoaderImmediately(null);
                GameManager.Instance.modalWindow.Init(ModalWindowMode.Error, $"Произошла ошибка. {userResponse.Error}",
                    null);
                GameManager.Instance.modalWindow.gameObject.SetActive(true);
            }
        });
    }

    public void OnClickClose()
    {
        GameManager.Instance.signInPanel.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}