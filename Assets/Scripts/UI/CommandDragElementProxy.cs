﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CommandDragElementProxy : CommandDragElement
{
    [SerializeField] private CommandDragElement _element;

    public override void Init(Transform idleParent, Transform dragParent, CommandNode node)
    {
        base.Init(idleParent, dragParent, node);
        _element.Init(transform, dragParent, node);
        _sprite.sprite = node.sprite;
        _sprite.color = node.color;
    }

    public override void OnDrag(PointerEventData eventData)
    {
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
    }
}
