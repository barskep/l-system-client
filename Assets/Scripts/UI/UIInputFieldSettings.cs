﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInputFieldSettings : MonoBehaviour
{
    [SerializeField] private int _minValue;
    [SerializeField] private int _maxValue;
    [SerializeField] private InputField _inputField;

    public void OnValueChanged()
    {
        int outVal = 0;
        if (int.TryParse(_inputField.text, out outVal))
        {
            if (outVal < _minValue)
                _inputField.text = _minValue.ToString();

            if (outVal > _maxValue)
                _inputField.text = _maxValue.ToString();
        }
    }
}
