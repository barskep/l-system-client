﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandDragPanel : MonoBehaviour
{
    [SerializeField] private CommandDragElementProxy _elementPrefab;
    [SerializeField] private CommandDragSettings _dragSettings;
    [SerializeField] private Transform _elementIdleParent;
    [SerializeField] private Transform _elementDragParent;

    private void Start()
    {
        foreach (var node in _dragSettings.Nodes)
        {
            var drag = Instantiate(_elementPrefab, _elementIdleParent);
            drag.Init(_elementIdleParent, _elementDragParent, node);
        }

        _elementPrefab.gameObject.SetActive(false);
    }
}
