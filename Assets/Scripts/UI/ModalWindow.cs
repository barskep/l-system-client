﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModalWindow : MonoBehaviour
{
    [SerializeField] private Image _icon;
    [SerializeField] private Text _text;
    [SerializeField] private Sprite _acceptSprite;
    [SerializeField] private Sprite _errorSprite;
    private ModalWindowMode _mode;
    private Action _onClickOk;

    public void Init(ModalWindowMode mode, string message, Action onClickOk)
    {
        _mode = mode;
        _onClickOk = onClickOk;
        _text.text = message;

        switch (_mode)
        {
            case ModalWindowMode.Error:
                _icon.sprite = _errorSprite;
                break;
            case ModalWindowMode.Accept:
                _icon.sprite = _acceptSprite;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void OnClickOk()
    {
        _onClickOk?.Invoke();
        gameObject.SetActive(false);
    }
}

public enum ModalWindowMode
{
    Error, Accept
}
