﻿using System.Collections;
using System.Collections.Generic;
using Domain;
using UnityEngine;
using UnityEngine.UI;

public class ResultItem : MonoBehaviour
{
    [SerializeField] private Text _text;

    public void Init(Decision decision, List<User> users)
    {
        var user = users?.Find(t => t.id == decision.createdBy);
        var userName = user == null ? "" : user.name;
        _text.text = userName + " Задание " + decision.taskId + " - " + decision.grade;
    }
}