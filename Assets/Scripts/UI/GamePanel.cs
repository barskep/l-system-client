﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Domain;
using UnityEngine;
using UnityEngine.UI;

public class GamePanel : MonoBehaviour
{
    [SerializeField] private GameFieldSettingsPanel _gameFieldSettingsPanel;
    [SerializeField] private CommandDragPanel _commandDragPanel;
    [SerializeField] private Button _createTaskButton;
    [SerializeField] private Button _uploadDecision;
    [SerializeField] private Button _estimateButton;
    [SerializeField] private MarkPanel _markPanel;
    [SerializeField] private Text _textPanel;
    [SerializeField] private Slider _slider;
    [SerializeField] private Text _sliderText;

    public GamePanelMode GamePanelMode { get; private set; }

    public void CreateRandomField()
    {
        _gameFieldSettingsPanel.OnClickGenerate();
    }

    public void Init(GamePanelMode mode)
    {
        GamePanelMode = mode;
        _estimateButton.gameObject.SetActive(false);
        _markPanel.gameObject.SetActive(false);
        _gameFieldSettingsPanel.gameObject.SetActive(false);
        _commandDragPanel.gameObject.SetActive(false);
        _createTaskButton.gameObject.SetActive(false);
        _uploadDecision.gameObject.SetActive(false);
        _textPanel.gameObject.SetActive(false);
        GameManager.Instance.algorithmPanel.Clear();
        if (mode != GamePanelMode.CheckDecision)
        {
            _commandDragPanel.gameObject.SetActive(true);
        }
        else
        {
            _textPanel.gameObject.SetActive(true);
            var task = GameManager.Instance.currentTask;
            _textPanel.text = "Задание №" + task.taskId + ": " + task.text;
            _estimateButton.gameObject.SetActive(true);
        }

        if (mode == GamePanelMode.CreatingTask)
        {
            _gameFieldSettingsPanel.gameObject.SetActive(true);
            _createTaskButton.gameObject.SetActive(true);
        }

        if (mode == GamePanelMode.TaskPerformance)
        {
            _textPanel.gameObject.SetActive(true);
            var task = GameManager.Instance.currentTask;
            _textPanel.text = "Задание №" + task.taskId + ": " + task.text;
            _uploadDecision.gameObject.SetActive(true);
        }
    }

    public void OnClickCreateTask()
    {
        var taskText = _gameFieldSettingsPanel.TaskText;
        var gameFieldtxt = _gameFieldSettingsPanel.GetGameField(GameManager.Instance.gameField.Squares);
        var gameField = string.Join("&", _gameFieldSettingsPanel.GetGameField(GameManager.Instance.gameField.Squares));
        var teacherId = GameManager.Instance.teacherMenuPanel.teacher.id;
        if (string.IsNullOrEmpty(taskText))
        {
            GameManager.Instance.modalWindow.Init(ModalWindowMode.Error, "Перед отправкой введите текст задания!", null);
            GameManager.Instance.modalWindow.gameObject.SetActive(true);
            return;
        }
        GameManager.Instance.OpenLoader();
        GameManager.Instance.serverApi.CreateTask(taskText, gameField, teacherId, response =>
        {
            if (!response.IsError)
            {
                GameManager.Instance.CloseLoaderWithDelay(() =>
                {
                    GameManager.Instance.teacherMenuPanel.gameObject.SetActive(true);
                    GameManager.Instance.modalWindow.gameObject.SetActive(true);
                    GameManager.Instance.modalWindow.Init(ModalWindowMode.Accept, "задание успешно отправлено!", null);
                    gameObject.SetActive(false);
                });
            }
        });
    }

    public void OnClickClose()
    {
        if (GamePanelMode == GamePanelMode.TaskPerformance)
            GameManager.Instance.pupilMenuPanel.gameObject.SetActive(true);
        else GameManager.Instance.teacherMenuPanel.gameObject.SetActive(true);

        gameObject.SetActive(false);
    }

    public void OnClickUploadDecision()
    {
        var decision = new Decision()
        {
            program = GameManager.Instance.startCell.CurrentCommand.GenerateCommandCode(),
            createdBy = GameManager.Instance.currentUser.id,
            teacherId = GameManager.Instance.currentUser.teacherId,
            taskId = GameManager.Instance.currentTask.taskId.ToString()
        };
        GameManager.Instance.OpenLoader();
        GameManager.Instance.serverApi.SendSolution(decision, response =>
        {
            if (!response.IsError)
            {
                GameManager.Instance.CloseLoaderImmediately(() =>
                {
                    GameManager.Instance.modalWindow.Init(ModalWindowMode.Accept, "Решение успешно отправлено!", null);
                    GameManager.Instance.modalWindow.gameObject.SetActive(true);
                });
            }
        });
    }

    public void OnSliderValueChange()
    {
        _sliderText.text = "X" + _slider.value;
        GameManager.Instance.timeCoefficient = _slider.value;
    }

    public void OnClickEstimate()
    {
        _markPanel.gameObject.SetActive(true);
    }

    public string[] GetGameField(IGameSquare[,] squares)
    {
        return _gameFieldSettingsPanel.GetGameField(squares);
    }

}

public enum GamePanelMode
{
    TaskPerformance,
    CreatingTask,
    CheckDecision
}