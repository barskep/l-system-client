﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultCommand : Command
{
    public override void AddCommandsToQueue(Queue<DirectionInfo> commands)
    {
        if (commandType == CommandType.CycleFor)
            throw new Exception("CycleForCommand can't be default!");

        if (commandType == CommandType.Down)
            commands.Enqueue(new DirectionInfo(Direction.Down, 1));
        if (commandType == CommandType.Left)
            commands.Enqueue(new DirectionInfo(Direction.Left, 1));
        if (commandType == CommandType.Right)
            commands.Enqueue(new DirectionInfo(Direction.Right, 1));
        if (commandType == CommandType.Up)
            commands.Enqueue(new DirectionInfo(Direction.Up, 1));
        if (commandType == CommandType.JumpDown)
            commands.Enqueue(new DirectionInfo(Direction.Down, 2));
        if (commandType == CommandType.JumpLeft)
            commands.Enqueue(new DirectionInfo(Direction.Left, 2));
        if (commandType == CommandType.JumpRight)
            commands.Enqueue(new DirectionInfo(Direction.Right, 2));
        if (commandType == CommandType.JumpUp)
            commands.Enqueue(new DirectionInfo(Direction.Up, 2));
        if (nextCell == null || nextCell.CurrentCommand == null)
            return;
        nextCell.CurrentCommand.AddCommandsToQueue(commands);
    }
}
