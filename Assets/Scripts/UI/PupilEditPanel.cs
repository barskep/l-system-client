﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Server;
using UnityEngine;

public class PupilEditPanel : PupilRegistrationPanel
{
    private User _pupil;

    public void Init(User user)
    {
        _pupil = user;
        _nameField.text = user.name;
        _loginField.text = user.username;
        _passField.text = user.password;
        _infoText.text = user.info;
    }

    public void OnClickEdit()
    {
        GameManager.Instance.serverApi.EditStudent(_pupil.id,
            _loginField.text.Trim().Length == 0 ? _pupil.username : _loginField.text.Trim(),
            _nameField.text.Trim().Length == 0 ? _pupil.name : _nameField.text.Trim(),
            _passField.text.Trim().Length == 0 ? _pupil.username : _loginField.text.Trim(),
            _infoText.text, response =>
            {
                if (!response.IsError)
                {
                    GameManager.Instance.teacherMenuPanel.pupils = GameManager.Instance.teacherMenuPanel.pupils
                        .Where((user) => user.id != _pupil.id).ToList();
                    GameManager.Instance.teacherMenuPanel.pupils.Add(response.Data);
                    GameManager.Instance.teacherMenuPanel.Init(GameManager.Instance.teacherMenuPanel.teacher,
                        GameManager.Instance.teacherMenuPanel.pupils);
                    GameManager.Instance.teacherMenuPanel.gameObject.SetActive(true);
                    gameObject.SetActive(false);
                }
                else
                {
                    GameManager.Instance.CloseLoaderImmediately(null);
                    GameManager.Instance.modalWindow.Init(ModalWindowMode.Error, $"Произошла ошибка. {response.Error}",
                        null);
                    GameManager.Instance.modalWindow.gameObject.SetActive(true);

                }
            });
    }

    public void Close()
    {
        GameManager.Instance.pupilInfoPanel.gameObject.SetActive(true);
        GameManager.Instance.pupilInfoPanel.Init(_pupil);
        gameObject.SetActive(false);
    }
}