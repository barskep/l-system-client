﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

public abstract class Command : DragElement
{
    [SerializeField] protected Cell nextCell;
    [SerializeField] protected Cell subCell;
    [SerializeField] protected TextMeshProUGUI commandText;
    [SerializeField] protected Image _sprite;
    [SerializeField] protected Color notInAlgorithmColor;
    [SerializeField] private Button _deleteButton;
    protected Cell prevCell;
    public Cell NextCommand
    {
        get => nextCell;
        set => nextCell = value;
    }

    protected bool inAlgorithm;

    public bool InAlgorithm
    {
        get => inAlgorithm;
        set
        {
            inAlgorithm = value;
            if (nextCell != null)
                nextCell.InAlgorithm = inAlgorithm;

            if (subCell != null)
                subCell.InAlgorithm = inAlgorithm;
            
            _sprite.color = inAlgorithm ? node.color : notInAlgorithmColor;
            _deleteButton.gameObject.SetActive(!inAlgorithm);
        }
    }


    public Cell SubCommand
    {
        get => subCell;
        set => subCell = value;
    }

    protected CommandType commandType;

    public CommandType CommandType => commandType;

    protected CommandNode node;
    public CommandNode Node => node;

    public void Init(CommandNode node, Cell prevCell, bool inAlgorithm)
    {
        this.prevCell = prevCell;
        commandType = node.type;
        this.node = node;
        commandText.text = node.commandName;
        _sprite.sprite = node.sprite;
        _sprite.color = node.color;
        InAlgorithm = inAlgorithm;
        Init(transform.parent, GameManager.Instance.parentForCommands);

        if (!GameManager.Instance.commands.ContainsKey(gameObject))
            GameManager.Instance.commands.Add(gameObject, this);
    }

    public virtual string GenerateCommandCode()
    {
        string subCommands = subCell == null ? "" : "{&" + subCell.GetCommandCode() + "}&";
        string nextCommands = nextCell == null ? "" : nextCell.GetCommandCode();
        return commandType.ToString() + "&" + subCommands + nextCommands;
    }

    public void DeleteCommand()
    {
        GameManager.Instance.commands.Remove(gameObject);
        if (nextCell != null && nextCell.CurrentCommand != null)
            nextCell.CurrentCommand.DeleteCommand();
        
        if (subCell != null && subCell.CurrentCommand != null)
            subCell.CurrentCommand.DeleteCommand();
        Destroy(gameObject);
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);

        if (prevCell != null)
        {
            prevCell.CurrentCommand = null;
            prevCell = null;
        }

        InAlgorithm = false;
    }
    

    public override void OnEndDrag(PointerEventData eventData)
    {
        _canvasGroup.blocksRaycasts = true;
    }

    public abstract void AddCommandsToQueue(Queue<DirectionInfo> commands);

}

public enum CommandType
{
    Up, Down, Right, Left, JumpUp, JumpDown, JumpRight, JumpLeft, CycleFor
}
