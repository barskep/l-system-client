﻿using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine;

public class CycleForCommand : Command
{
    [SerializeField] private TMP_InputField _cycleCounts;
    public override string GenerateCommandCode()
    {
        var subCommands = subCell == null
            ? "{&}&"
            : "{&" + subCell.GetCommandCode() + "}&";
        var nextCommands = nextCell == null ? "" : nextCell.GetCommandCode();

        return "CycleFor " + int.Parse(_cycleCounts.text) + " &" + subCommands + nextCommands;

    }

    public void SetCycles(int cycles)
    {
        _cycleCounts.text = cycles.ToString();
    }

    public override void AddCommandsToQueue(Queue<DirectionInfo> commands)
    {
        for (var i = 0; i < int.Parse(_cycleCounts.text); i++)
        {
            if (subCell != null && subCell.CurrentCommand != null)
                subCell.CurrentCommand.AddCommandsToQueue(commands);
        }

        if (nextCell != null && nextCell.CurrentCommand != null)
            nextCell.CurrentCommand.AddCommandsToQueue(commands);
    }
}
