﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellYRepeater : MonoBehaviour
{
    [SerializeField] private Cell _targetCell;
    private float _yOffset;

    private void Start()
    {
        _yOffset = transform.position.y - _targetCell.transform.position.y;
        //GameManager.Instance.OnCommandsUpdate += OnTargetCellCommandChanged;
    }

    private void Update()
    {
        OnTargetCellCommandChanged();
    }
    
    private void OnTargetCellCommandChanged()
    {
        var newPos = new Vector3(transform.position.x, _yOffset + _targetCell.GetBottomCellPosition().y,
            transform.position.z);
        transform.position = newPos;
        //if (GameManager.Instance.OnCommandsUpdate != null)
            //GameManager.Instance.OnCommandsUpdate();
    }
}
