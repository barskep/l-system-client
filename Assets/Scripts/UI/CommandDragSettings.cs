﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Command Drag Settings", menuName = "Setting Objects/Command Drag Settings")]
public class CommandDragSettings : ScriptableObject
{
    [SerializeField] private List<CommandNode> _nodes;
    public List<CommandNode> Nodes => _nodes;
}

[System.Serializable]
public class CommandNode
{
    public Sprite sprite;
    public Color color;
    public CommandType type;
    public Command prefab;
    public string commandName;
}
