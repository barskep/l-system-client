﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PupilInfoPanel : MonoBehaviour
{
    [SerializeField] private Text _labelText;
    [SerializeField] private Text _infoText;
    private User _pupil;

    public void Init(User user)
    {
        _labelText.text = "Информация об ученике по имени: " + user.name;
        _infoText.text = user.info;
        _pupil = user;
    }

    public void OnClickCheckTask()
    {
        GameManager.Instance.decisionsPanel.gameObject.SetActive(true);
        GameManager.Instance.serverApi.GetTasks(GameManager.Instance.teacherMenuPanel.teacher.id, response =>
        {
            if (!response.IsError)
            {
                GameManager.Instance.serverApi.GetSolutions(GameManager.Instance.teacherMenuPanel.teacher.id,
                    response1 =>
                    {
                        if (!response1.IsError)
                        {
                            GameManager.Instance.decisionsPanel.Init(response1.Data.items.ToList(),
                                response.Data.items.ToList(), int.Parse(_pupil.id));
                        }
                        else
                        {
                            GameManager.Instance.CloseLoaderImmediately(null);
                            GameManager.Instance.modalWindow.Init(ModalWindowMode.Error,
                                $"Произошла ошибка. {response1.Error}", null);
                            GameManager.Instance.modalWindow.gameObject.SetActive(true);

                        }
                    });
            }
            else
            {
                GameManager.Instance.CloseLoaderImmediately(null);
                GameManager.Instance.modalWindow.Init(ModalWindowMode.Error, $"Произошла ошибка. {response.Error}",
                    null);
                GameManager.Instance.modalWindow.gameObject.SetActive(true);

            }
        });
    }

    public void OnClickStats()
    {
        GameManager.Instance.statsPanel.gameObject.SetActive(true);
        GameManager.Instance.serverApi.GetSolutions(GameManager.Instance.teacherMenuPanel.teacher.id,
            response1 =>
            {
                if (!response1.IsError)
                {
                    var list = response1.Data.items.ToList();
                    list.RemoveAll(t => int.Parse(t.createdBy) != int.Parse(_pupil.id));
                    GameManager.Instance.statsPanel.Init(list, new List<User>() {_pupil});
                }
            });
    }

    public void OnClickEdit()
    {
        GameManager.Instance.pupilEditPanel.gameObject.SetActive(true);
        GameManager.Instance.pupilEditPanel.Init(_pupil);
        gameObject.SetActive(false);
    }

    public void OnClickDelete()
    {
        GameManager.Instance.serverApi.DeleteUser(_pupil.id, response =>
        {
            if (!response.IsError)
            {
                GameManager.Instance.teacherMenuPanel.pupils = GameManager.Instance.teacherMenuPanel.pupils
                    .Where((user) => user.id != _pupil.id).ToList();
                GameManager.Instance.teacherMenuPanel.Init(GameManager.Instance.teacherMenuPanel.teacher,
                    GameManager.Instance.teacherMenuPanel.pupils);
                GameManager.Instance.teacherMenuPanel.gameObject.SetActive(true);
                gameObject.SetActive(false);
            }
            else
            {
                GameManager.Instance.CloseLoaderImmediately(null);
                GameManager.Instance.modalWindow.Init(ModalWindowMode.Error, $"Произошла ошибка. {response.Error}",
                    null);
            }
        });
    }

    public void Close()
    {
        GameManager.Instance.teacherMenuPanel.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}