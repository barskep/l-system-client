﻿using System.Collections;
using System.Collections.Generic;
using Domain;
using UnityEngine;

public class PupilDecisionsPanel : MonoBehaviour
{
    [SerializeField] private DecisionScroll _scroll;

    public void Init(List<Decision> decisions, List<Task> tasks, int userId)
    {
        decisions.RemoveAll(t => int.Parse(t.createdBy) != userId);
        _scroll.Init(decisions, tasks);
    }

    public void OnClickClose()
    {
        GameManager.Instance.pupilInfoPanel.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}
