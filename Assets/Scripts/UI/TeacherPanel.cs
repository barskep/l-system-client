﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TeacherPanel : MonoBehaviour
{
    [SerializeField] private Text _teacherHello;
    [SerializeField] private PupilItemButton _pupilItemPrefab;
    [SerializeField] private Transform _pupilsTransform;
    private List<PupilItemButton> _pupils;

    public User teacher;
    public List<User> pupils = new List<User>();

    public void StartUpdate()
    {
        GameManager.Instance.serverApi.GetStudents(teacher.id, response =>
        {
            if (!response.IsError)
            {
                GameManager.Instance.CloseLoaderWithDelay(() =>
                {
                    Debug.Log(teacher.id);
                    pupils = new List<User>(response.Data.items);

                    Init(teacher, pupils);
                });
            }
            else
            {
                GameManager.Instance.CloseLoaderImmediately(null);
                GameManager.Instance.modalWindow.Init(ModalWindowMode.Error, $"Произошла ошибка. {response.Error}",
                    null);
                GameManager.Instance.modalWindow.gameObject.SetActive(true);

            }
        });
    }

    public void SetTeacher(User teacher)
    {
        this.teacher = teacher;
    }

    public void Init(User teacher, List<User> pupils)
    {
        if (_pupils != null && _pupils.Count > 0)
            foreach (var pupil in _pupils)
                Destroy(pupil.gameObject);

        _pupils = new List<PupilItemButton>();

        _pupilItemPrefab.gameObject.SetActive(true);
        foreach (var pupil in pupils)
        {
            Debug.Log(pupil.id);
            var item = Instantiate(_pupilItemPrefab, _pupilsTransform);
            item.Init(pupil);
            _pupils.Add(item);
        }


        _pupilItemPrefab.gameObject.SetActive(false);


        _teacherHello.text = "Добро пожаловать, " + teacher.name + "! ";
    }

    public void OnClickAddPupil()
    {
        GameManager.Instance.pupilRegisterPanel.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }

    public void OnClickSetTask()
    {
        GameManager.Instance.OpenAddTask();
        gameObject.SetActive(false);
    }

    public void OnClickViewStats()
    {
        GameManager.Instance.statsPanel.gameObject.SetActive(true);
        GameManager.Instance.serverApi.GetSolutions(GameManager.Instance.teacherMenuPanel.teacher.id,
            response1 =>
            {
                if (!response1.IsError)
                {
                    var list = response1.Data.items.ToList();
                    list.RemoveAll(t => int.Parse(t.teacherId) != int.Parse(teacher.id));
                    GameManager.Instance.statsPanel.Init(list, pupils);
                }
            });
    }

    public void OnClickExit()
    {
        GameManager.Instance.signInPanel.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}