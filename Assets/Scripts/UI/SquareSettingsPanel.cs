﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SquareSettingsPanel : MonoBehaviour
{
    [SerializeField] private Dropdown _type;
    [SerializeField] private Button _addStone;
    [SerializeField] private Button _deleteStone;
    [SerializeField] private Button _moveCharacter;
    [SerializeField] private FieldObjectSettings _fieldObjectSettings;
    [SerializeField] private GameSquareSettings _gameSquareSettings;
    private GameSquare _square;

    public void Init(GameSquare square)
    {
        _square = square;
        _addStone.gameObject.SetActive(false);
        _deleteStone.gameObject.SetActive(false);
        _moveCharacter.gameObject.SetActive(false);
        _type.enabled = false;
        _type.value = (int)square.SquareType;
        _type.enabled = true;
        
        if (square.MovableObjectType == MovableObjectType.None)
        {
            _moveCharacter.gameObject.SetActive(square.SquareType != SquareType.Hole);
            _addStone.gameObject.SetActive(square.SquareType == SquareType.Default);
        }
        else if (square.MovableObjectType == MovableObjectType.Stone)
            _deleteStone.gameObject.SetActive(true);
        GameManager.Instance.gameField.UpdateField();
    }

    public void OnDropDownValueChanged()
    {
        if (_type.value == 0)
        {
            _square.Init(_gameSquareSettings.nodes.Find(t => t.type == SquareType.Default), _square.PosI, _square.PosJ);
        }
        else if (_type.value == 1)
        {
            if (_square.MovableObjectType == MovableObjectType.Stone)
            {
                GameManager.Instance.gameField.DestroyMovable(_square.movableObject);
                _square.movableObject = null;
            }


            _square.Init(_gameSquareSettings.nodes.Find(t => t.type == SquareType.Busy), _square.PosI, _square.PosJ);
        }
        else if (_type.value == 2)
        {
            if (_square.movableObject != null)
                GameManager.Instance.gameField.DestroyMovable(_square.movableObject);

            _square.Init(_gameSquareSettings.nodes.Find(t => t.type == SquareType.Hole), _square.PosI, _square.PosJ);
        }

        Init(_square);
    }

    public void OnClickAddStone()
    {
        GameManager.Instance.gameField.InstantiateStone(_square.PosI, _square.PosJ);
        Init(_square);
    }

    public void OnClickDeleteStone()
    {
        GameManager.Instance.gameField.DestroyMovable(_square.movableObject);
        _square.movableObject = null;
        Init(_square);
    }

    public void OnClickMoveCharacter()
    {
        GameManager.Instance.gameField.MoveCharacter(_square.PosI, _square.PosJ);
        Init(_square);
    }

    public void OnClickClose()
    {
        gameObject.SetActive(false);
    }
}
