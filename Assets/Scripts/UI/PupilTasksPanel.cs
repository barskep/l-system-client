﻿using System.Collections;
using System.Collections.Generic;
using Domain;
using UnityEngine;

public class PupilTasksPanel : MonoBehaviour
{
    [SerializeField] private TasksScroll _tasksScroll;
    private List<Task> _tasks;

    public void StartUpdate(User pupil)
    {
        GameManager.Instance.serverApi.GetTasks(pupil.teacherId, response =>
        {
            if (!response.IsError)
            {
                _tasks = new List<Task>(response.Data.items);
                Init(_tasks);
            }
            else
            {
                GameManager.Instance.CloseLoaderImmediately(null);
                GameManager.Instance.modalWindow.Init(ModalWindowMode.Error, $"Произошла ошибка. {response.Error}", null);
                GameManager.Instance.modalWindow.gameObject.SetActive(true);

            }
        });
    }
        
    
    
    public void Init(List<Task> tasks)
    {
        _tasks = tasks;
        _tasksScroll.Init(tasks);
    }

    public void Close()
    {
        GameManager.Instance.pupilMenuPanel.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}
