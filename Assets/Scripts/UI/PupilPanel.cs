﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Domain;
using UnityEngine;
using UnityEngine.UI;

public class PupilPanel : MonoBehaviour
{
    [SerializeField] private Text _helloText;
    private User _pupil;

    public void Init(User user)
    {
        _pupil = user;
        GameManager.Instance.currentUser = user;
        GameManager.Instance.CloseLoaderWithDelay(null, 0.5f);
        _helloText.text = "Добро пожаловать, " + user.name + "!";
    }

    public void OnClickCheckTasks()
    {
        GameManager.Instance.pupilTasksPanel.gameObject.SetActive(true);

        GameManager.Instance.pupilTasksPanel.StartUpdate(_pupil);
        gameObject.SetActive(false);
    }

    public void OnClickStats()
    {
        GameManager.Instance.statsPanel.gameObject.SetActive(true);
        GameManager.Instance.serverApi.GetSolutions(_pupil.teacherId,
            response1 =>
            {
                if (!response1.IsError)
                {
                    var list = response1.Data.items.ToList();
                    list.RemoveAll(t => int.Parse(t.createdBy) != int.Parse(_pupil.id));
                    GameManager.Instance.statsPanel.Init(list, new List<User>() {_pupil});
                }
            });
    }

    public void Exit()
    {
        GameManager.Instance.signInPanel.gameObject.SetActive(true);
        gameObject.SetActive(false);

        //Application.Quit();
    }
}