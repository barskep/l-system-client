﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class PupilItemButton : MonoBehaviour
{
    [SerializeField] private Text _name;
    private User _pupil;

    public void Init(User pupil)
    {
        _pupil = pupil;
        _name.text = pupil.name;
    }

    public void OnClickPupil()
    {
        GameManager.Instance.pupilInfoPanel.gameObject.SetActive(true);
        GameManager.Instance.pupilInfoPanel.Init(_pupil);
        GameManager.Instance.teacherMenuPanel.gameObject.SetActive(false);
    }
}
