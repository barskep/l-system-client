﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITextSlider : MonoBehaviour
{
    [SerializeField] private Slider _slider;
    [SerializeField] private Text _text;

    private void Start()
    {
        _text.text = "0%";
    }
    
    public void OnValueChanged()
    {
        _text.text = _slider.value + "%";
    }
}
