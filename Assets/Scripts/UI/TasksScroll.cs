﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Domain;
using UnityEngine;

public class TasksScroll : MonoBehaviour
{
    [SerializeField] private TaskItem _prefab;
    [SerializeField] private Transform _parent;
    private List<TaskItem> _items;

    public void Init(List<Task> tasks)
    {
        _prefab.gameObject.SetActive(true);
        if (_items == null)
            _items = new List<TaskItem>();
        foreach (var item in _items)
        {
            Destroy(item.gameObject);
        }
        _items.Clear();
        foreach (var task in tasks)
        {
            var item = Instantiate(_prefab, _parent);
            _items.Add(item);
            item.Init(task);
        }

        _prefab.gameObject.SetActive(false);
    }
}
