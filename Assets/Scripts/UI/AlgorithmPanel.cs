﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlgorithmPanel : MonoBehaviour
{
    [SerializeField] private CommandDragSettings _commandDragSettings;

    public void Clear()
    {
        var startCell = GameManager.Instance.startCell;
        if (startCell.CurrentCommand != null)
            startCell.CurrentCommand.DeleteCommand();
    }
    public void Init(string baseCode)
    {
        var code = baseCode.Split('&');
        var startCell = GameManager.Instance.startCell;
        Clear();
        var compileInfo = new CompileInfo(0, startCell);
        while (compileInfo.index < code.Length)
        {
            compileInfo = GenerateSubCommandRecursively(code, compileInfo);
            compileInfo.index++;
        }
    }

    private CompileInfo GenerateSubCommandRecursively(string[] code, CompileInfo info)
    {
        var i = info.index;
        while (i < code.Length && code[i] != "}")
        {
            if (string.IsNullOrEmpty(code[i]))
            {
                i++;
                continue;
            }
            var node = GetCommandNode(code[i]);
            var command = Instantiate(node.prefab, info.baseCell.transform);
            command.Init(node, info.baseCell, true);
            info.baseCell.CurrentCommand = command;
            info.baseCell = command.NextCommand;
            i++;
            if (node.type == CommandType.CycleFor)
            {
                ((CycleForCommand)command).SetCycles(int.Parse(code[i - 1].Split(' ')[1]));
                i = GenerateSubCommandRecursively(code,  new CompileInfo(i + 1, command.SubCommand)).index + 1;
            }
        }

        info.index = i;
        return info;
    }

    private CommandNode GetCommandNode(string command)
    {
        CommandType type;
        if (Enum.TryParse(command, out type))
        {
            return _commandDragSettings.Nodes.Find(t => t.type == type);
        }
        else if (command.Contains("CycleFor"))
        {
            return _commandDragSettings.Nodes.Find(t => t.type == CommandType.CycleFor);
        }
        throw new Exception("Unknown CommandType " + command);
    }
}

public class CompileInfo
{
    public int index;
    public Cell baseCell;

    public CompileInfo(int index, Cell baseCell)
    {
        this.index = index;
        this.baseCell = baseCell;
    }
}

