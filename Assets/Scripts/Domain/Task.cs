﻿using System;

namespace Domain
{
    [Serializable]
    public class Task
    {
        public int taskId;
        public string text;
        public string[] gameField;
    }
}