﻿namespace Domain
{
    public class Result
    {
        public string pupilId;
        public string pupilName;
        public string teacherId;
        public string taskId;
        public string mark;
    }
}