﻿using System;

namespace Domain
{
    [Serializable]
    public class Decision
    {
        public int solutionId;
        public string program;
        public string createdBy;
        public string teacherId;
        public string taskId;
        public int grade;
    }
}