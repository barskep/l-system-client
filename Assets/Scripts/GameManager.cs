﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain;
using Server;
using UnityEngine;
using UnityEngine.Serialization;

public class GameManager : MonoBehaviour
{
    #region  Singleton
    public static GameManager Instance { get; private set; }
    #endregion

    public ServerApi serverApi;
    public Dictionary<GameObject, Command> commands;
    public Dictionary<GameObject, Cell> cells;
    public Dictionary<GameObject, CommandDragElement> dragElements;
    public GameField gameField;
    public Cell startCell;
    public Transform parentForCommands;
    public Action OnCommandsUpdate;
    public Action OnGameFieldError;
    public MovableObject character;
    public float timeCoefficient = 1;
    public GamePanel gamePanel;
    public SquareSettingsPanel squareSettingsPanel;
    [FormerlySerializedAs("_algorithmPanel")] public AlgorithmPanel algorithmPanel;
    public SignInPanel signInPanel;
    public RegisterPanel registerPanel;
    public TeacherPanel teacherMenuPanel;
    public PupilRegistrationPanel pupilRegisterPanel;
    public PupilInfoPanel pupilInfoPanel;
    public PupilEditPanel pupilEditPanel;
    public StatsPanel statsPanel;
    public PupilPanel pupilMenuPanel;
    public PupilTasksPanel pupilTasksPanel;
    public PupilDecisionsPanel decisionsPanel;
    public InfoPanel infoPanel;
    public MarkPanel markPanel;
    public ModalWindow modalWindow;
    public GameObject _loader;
    public Task currentTask;
    public User currentUser;
    public Decision currentDecision;
    private string _compileString;

    private string[] strings = new[] {"d d d d d d d d d d d d d d d", "d d d d d d d d d d d d d d d", "d d d d d d d d d d d d d d d", "d d d d d d d d d d d d d d d", "d d d d d d d d d d d d d d d",
        "d d d d d d d d d d d d d d d", "d d d d d d d d d d d d d d d", "d d d d d d d d d d d d d d d", "d d d d d d d d d d d d d d d", "d d d d d d d d d d d d d d d", "d d d d d d d d d d d d d d d",
        "d d d d d d d d d d d d d d d", "d d d d d d d d d d d d d d d", "d d d d d d d d d d d d d d d", "d d d d d d d d d d d d d d d", "d d d d d d d d d d d d d d d",
    };

    private void Awake()
    {
        Instance = this;
        commands = new Dictionary<GameObject, Command>();
        cells = new Dictionary<GameObject, Cell>();
        dragElements = new Dictionary<GameObject, CommandDragElement>();
        gameField.CreateField(strings);
        startCell.InAlgorithm = true;
        serverApi = new ServerApi();
        OnGameFieldError += OnGameError;
    }

    public void OnGameError()
    {
        modalWindow.gameObject.SetActive(true);
        modalWindow.Init(ModalWindowMode.Error, "Муравей не может сюда пройти!", null);
        gameField.RestartLevel();
    }

    private void Start()
    {
        gamePanel.gameObject.SetActive(false);
        signInPanel.gameObject.SetActive(true);
        registerPanel.gameObject.SetActive(false);

        //TODO: remove after debug
        //DebugOpen();
    }

    public void OnClickCompile()
    {
        if (startCell.CurrentCommand != null)
        {
            var queue = new Queue<DirectionInfo>();
            startCell.CurrentCommand.AddCommandsToQueue(queue);
            foreach (var command in queue)
            {
                character.AddDirectionInfoToQueue(command);
            }
            _compileString = startCell.CurrentCommand.GenerateCommandCode();
            Debug.Log(_compileString);
        }
    }

    public void OnClickRebuildCode()
    {
        algorithmPanel.Init(_compileString);
    }

    public void OpenAddTask()
    {
        gameField.gameObject.SetActive(true);
        gamePanel.gameObject.SetActive(true);
        gamePanel.CreateRandomField();
        pupilTasksPanel.gameObject.SetActive(false);
        gamePanel.Init(GamePanelMode.CreatingTask);
    }

    public void OpenTask(Task task)
    {
        currentTask = task;
        gameField.gameObject.SetActive(true);
        gamePanel.gameObject.SetActive(true);
        gameField.CreateField(task.gameField);
        pupilTasksPanel.gameObject.SetActive(false);
        gamePanel.Init(GamePanelMode.TaskPerformance);
    }

    public void CheckDecision(Decision decision, Task task)
    {
        markPanel.SetValue(decision.grade);
        currentDecision = decision;
        currentTask = task;
        gameField.gameObject.SetActive(true);
        gamePanel.gameObject.SetActive(true);
        gameField.CreateField(task.gameField);
        gamePanel.Init(GamePanelMode.CheckDecision);
        algorithmPanel.Init(decision.program);
    }

    public void OpenLoader()
    {
        _loader.gameObject.SetActive(true);
    }

    public void CloseLoaderWithDelay(Action callback, float delay = 2)
    {
        StartCoroutine(LoaderClosing(callback, delay));
    }

    private IEnumerator LoaderClosing(Action callback, float delay)
    {
        yield return new WaitForSeconds(delay);
        CloseLoaderImmediately(callback);
    }

    public void CloseLoaderImmediately(Action callback)
    {
        _loader.gameObject.SetActive(false);
        callback?.Invoke();
    }
    
    
    
    private void DebugOpen()
    {
        gamePanel.gameObject.SetActive(false);
        registerPanel.gameObject.SetActive(false);
        pupilRegisterPanel.gameObject.SetActive(false);
        
        infoPanel.gameObject.SetActive(true);
        signInPanel.gameObject.SetActive(false);
        pupilInfoPanel.gameObject.SetActive(false);
        pupilEditPanel.gameObject.SetActive(false);
        teacherMenuPanel.gameObject.SetActive(false);
        pupilMenuPanel.gameObject.SetActive(false);
        
        /*

        var pupil = new User()
        {
            name = "Иванов Иван Иванович",
            login = "Piska",
            pass = "zzzz",
            info = "Крайне прилежный ученик",
            userType = UserType.Pupil
        };
        pupilMenuPanel.Init(pupil);*/
        
        /*var teacher = new User
        {
            name = "Иван Преподыч",
            userType = UserType.Teacher
        };

        var users = new User[]
        {
            new User()
            {
                name = "Долбик Васечкин",
                userType = UserType.Pupil
            },
            new User()
            {
                name = "Рубен Иванов",
                userType = UserType.Pupil
            },
            new User()
            {
                name = "Иван Рубенов",
                userType = UserType.Pupil
            }

        };

        teacherMenuPanel.Init(teacher, users);*/
    }
}
